+++
title = "Hew hew"
description = ""
weight = 2
+++
  # PHẦN II: HƯỚNG DẪN CÁC DẠNG SHADER GRAPH
  ***Từ khoá:***
- ***BlackBoard:*** bảng property chứa các thuộc tính mà có thể chỉnh sửa được tại cửa sổ inspector khi shader được sử dụng trên scene, thông thường bảng sẽ nằm ở góc trái màn hình chỉnh sửa shader.
- ___Preview:___ bảng cho phép xem trước hình dạng của shader, thường nằm góc phải màn hình chỉnh sửa.
- ___(1)___: kiểu vector1(x)
- ___(2)___:kiểu vector2()x,y
  
  ##  Mục Lục
  - [1 Portal Shader graph](#1-portal-shader-graph)
  - [2 Fire flames Shader graph ](#2-fire-flames-shader-graph) 

---

## 1 Portal Shader graph
  ###  Cổng dịch chuyển
  ### 1. Tạo  01 PBR Shader Graph đặt tên là PortalShader.
![Screenshot_28](image/ShaderPortal/Screenshot_1.png)
  ### 2. Thêm mới 01 node có kiểu là [Twirl](#1-twirl): 

![Screenshot_28](image/ShaderPortal/Screenshot_17.png)

  ###  3. Thêm mới 01 node có kiểu là **Vector1** và đặt tên là **TwirlStrength**: dùng để tùy chỉnh độ xoắn của hiệu ứng.

  ![Screenshot_28](image/ShaderPortal/Screenshot_18.png)

  **Thực hiện** kéo kết nối output  của vector1 vào Strength(1), sau đó chuột phải và ấn vào **Convert To Property** và đổi tên tại main workspace để thuộc tính trên có thể tùy chỉnh tại inspector.

  ###  4. Tiếp theo để tạo vòng xoắn thì chúng ta cần phải khiển giá trị offset của node [Twirl](#1-twirl) thay đổi theo giời gian, để làm được điều đó, thêm 1 node Time và 1 node **Speed** để điều chỉnh tốc độ thay đổi.
![Screenshot_28](image/ShaderPortal/Screeshot_1.png)

  1. để kết hợp thời gian và tốc độ, chúng ta thêm node [Multiply](#5-multiply) và kéo output của [Multiply](#5-multiply) vào offset trong twirl.

  ![Screenshot_28](image/ShaderPortal/Screenshot_21.png)

  ###  5. Thêm mới 01 node có kiểu là [Voronoi](#2-voronoi) để thêm các điểm trong 1 khoảng không có kích thước bằng nhau và độ dãn cách tương tác với nhau.

![Screenshot_28](image/ShaderPortal/Screenshot_19.png)

  ### 6. Tạo 1 thuộc tính trong **BlackBoard**  có kiểu là **vector1** để tên là **Power** dùng để điều chỉnh độ mạnh của giá trị vặn xoắn

![Screenshot_28](image/ShaderPortal/Power.png)

  1. thêm node Power và kéo  thuộc tính **power(1)** ra làm 1 node trên màn làm việc để điều chỉnh input B cho node **POwer**

![Screenshot_28](image/ShaderPortal/Screenshot_24.png)

### 7. Trong **master node** nhấn vào biểu tượng cài đặt và chọn **Two Sided** để shader hiển thị 2 mặt  

![Screenshot_28](image/ShaderPortal/main%20node.png)


### 8 như vậy chúng ta đã tạo xong màu xoắn ốc và có thể tùy chỉnh được speed, mật độ, tiếp theo chúng ta cần shader render theo 1 texture nhất định vì thế chúng ta thêm property **texture2d**  và truyền texture đó vào node [Sample texture2d](#4-sample-texture2d) sau đó kết nối tới emission và alpha trong master node.


![Screenshot_28](image/ShaderPortal/texture2d.png)

### 9 Sử dụng node  [Multiply](#5-multiply) để kết hợp giá trị của texture2d và giá trị vặn xoắn:


![Screenshot_28](image/ShaderPortal/multiply.png)

### 10 sau khi hoàn thành việc tạo hiệu ứng và truyền ảnh vào shader, chúng ta cần shader có thể tùy chỉnh được color vì thế thêm thành phần Color vào BlackBoard, khi đó, kéo Color thành 1 node và kết nối màu với output của texture vừa thực hiện phía trên bằng 1 node [Multiply](#5-multiply)
.


![Screenshot_28](image/ShaderPortal/Color.png)

### 11 Nhấn save asset để lưu và vào Scene tạo 1 quad sau đó cho shader vào để sử dụng.

![Screenshot_28](image/ShaderPortal/end.png)









## 2 Fire flames Shader graph 
### - hiệu ứng lửa cháy
### 1. tạo mới  1 PBR shader graph sau đó mở shader graph.

![Screenshot_28](image/ShaderFire/masternode.png)

- tạo Property **Texture2d**  và kéo ra làm node

![Screenshot_28](image/ShaderFire/11%20(2).png)

- tạo thêm node   [Sample texture2d](#4-sample-texture2d) để xuất **texture2d** trên ra màu rgba.

![Screenshot_28](image/ShaderFire/111111.png)

### 2. để tùy chỉnh màu cho shader, thêm 1 property Color tại BlackBoard và kéo ra làm node.

![Screenshot_28](image/ShaderFire/2%20(2).png) 
- trộn màu và texture2d vào với nhau bằng việc thêm 1 node [Multiply](#5-multiply).

 ![Screenshot_28](image/ShaderFire/2%20(1).png)

### 3. thêm node [Gradient Noise](#6-gradient-noise)  để tạo hiệu ứng đốm lửa
![image](image/ShaderFire/33333.png)

  - tại đây chúng ta sử dụng 1 mảng uv đầu vào trượt dọc theo vector định hướng Tiling trong node [Tiling and offset](#8-tiling-and-offset) , mật độ của các điểm noise được quy định bởi Scale, để node hoạt động thì cần truyền vào hệ thống màu thay đổi theo thời gian. thêm node time và node vector2 (để y bằng -0.2 để ảnh luôn trượt lên trên)để mix lại với nhau bằng multiply và tác động tới node tiling and offset như hình dưới, sau đó trả output của Gradient Noise vào UV trong node Sample texture2d.

![Screenshot_28](image/ShaderFire/3.png) 

### 4. Như vậy thì lửa sẽ cháy đều ở mọi vị trí của shader, vì thế chúng ta cần kiểm soát mức độ biến dạng của ngọn lửa bằng việc thêm 1 node  [Lerp](#7-lerp) và node  [UV](#10-uv), thêm property có kiểu vector1 để giới hạn giá trị nội suy giữa node  [UV](#10-uv) và node [Gradient Noise](#6-gradient-noise).

![Screenshot_28](image/ShaderFire/4.png) 

### 5. để tạo các hạt lửa bay lên theo ngọn lửa, cần tạo biến thời gian và vector 2 truyền ra [Tiling and offset](#8-tiling-and-offset)  như trên sau đó tạo thêm node [Voronoi](#2-voronoi) để được các hạt cảm giác bay lên trên

![Screenshot_28](image/ShaderFire/5.png) 

### 6. thêm node [Power](#3-power) vào cuối nhánh trên để tùy chỉnh độ lớn bé của tâm các hạt lưu ý mật độ các hạt không thay đổi, sau đấy mix  [Power](#3-power) với hiệu ứng [Gradient Noise](#6-gradient-noise) phía trên bằng  [Multiply](#5-multiply),  và đặt output ra master node như hình

![Screenshot_28](image/ShaderFire/6.png) 

### 7. khi đó chúng ta mix nhánh mà có texture 2d ở bước 4 vào nhánh vừa tạo được bằng 1 node  [Multiply](#5-multiply) để có được ảnh lửa cháy theo hình dạng texture2d truyền vào, cuối cùng là chỉnh màu cho hiệu ứng đó

![Screenshot_28](image/ShaderFire/7.png) | ![Screenshot_28](image/ShaderFire/77.png)

### 8. cuối cùng đặt các property cần tùy chỉnh vào BlackBoard để có thể chỉnh được trong inspector khi sử dụng shader

![Screenshot_28](image/ShaderFire/8.png)



  ## GIẢI THÍCH Ý NGHĨA CÁC NODE
  1. **định nghĩa**:
  
  1. **đặc điểm, tính chất**:

  1. **ý nghĩa các cổng**:

  1. **cách sử dụng:**

  1. **giải thích:**

  [Twirl](#1-twirl) | [Voronoi](#2-voronoi) | [Power](#3-power) | [Sample texture2d](#4-sample-texture2d) | [Multiply](#5-multiply)
  
  [Gradient Noise](#6-gradient-noise) |  [Lerp](#7-lerp) | [Tiling and offset](#8-tiling-and-offset) |  [Time](#9-time) |  [UV](#10-uv)

  



## 1 Twirl
  1. **định nghĩa**: là hiệu ứng tương tự như lỗ đen vặn xoắn vào tâm.
đặc điểm, tính chất: tạo ra 1 vòng xoắn UV theo 1 tâm dựa trên các thông số input
  1. **đặc điểm, tính chất**: tạo ra 1 vòng xoắn UV theo 1 tâm dựa trên các thông số input
  1. **ý nghĩa các cổng**:

|    Tên   |  Cổng  |   Kiểu   | ràng buộc kiểu   |        ý nghĩa       |
|:--------:|:------:|:--------:|:----------------:|:--------------------:|
|    UV    |  Input | Vector 2 |        UV        |  giá trị uv đầu vào  |
|  Center  |  Input | Vector 2 |       None       |      vị trí tâm      |
| Strength |  Input | Vector 1 |       None       | độ xoắn của hiệu ứng |
|  Offset  |  Input | Vector 2 |       None       |     chỉnh màu uv     |
|    Out   | Output | Vector 2 |       None       |   giá trị uv đầu ra  |

  1. **cách sử dụng:**
 nhập giá trị đầu vào Strength để điều chỉnh mức độ vặn xoắn của hiệu ứng
biến Offset để điều chỉnh giá trị tốc độ toàn bộ vòng xoắn.
khi đó đầu ra của node sẽ là giá trị UV có kiểu vector2
  1. **giải thích:** giá trị offset(2) thay đổi theo thời gian thì output của node trên cũng thanh đổi theo thời gian, khi đó uv đầu ra sẽ là hình xoắn ốc đang xoáy liên tục.

  ## 2 Voronoi

  1. **định nghĩa**:là 1 tập hợp các điểm trong 1 khoảng không có kích thước = nhau và độ dãn cách tương tác với nhau

  1. **đặc điểm, tính chất**:khi  kéo output  từ node **Twirl** sang uv của **Voronoi**, ta có các hạt trong 1 không gian bị vặn xoắn theo hình xoắn ốc.

  1. **ý nghĩa các cổng**:

|      Tên     |  Cổng  |   Kiểu   | ràng buộc kiểu   |                       ý nghĩa                      |
|:------------:|:------:|:--------:|:----------------:|:--------------------------------------------------:|
|      UV      |  Input | Vector 2 |        UV        |                 giá trị uv đầu vào                 |
| Angle Offset |  Input | Vector 1 |       None       | giá trị tương tác lên  các hạt xung quanh của điểm |
| Cell Density |  Input | Vector 1 |       None       |                   mật độ các điểm                  |
|      Out     | Output | Vector 1 |       None       |              giá trị noise đầu ra                  |
|     Cells    | Output | Vector 1 |       None       |             giá trị thô của điểm                   |

  1. **cách sử dụng:**cần có thêm 1 biến Vector1 để chỉnh **Cell Density** : mật độ các hạt và để tên Scale  trên **BlackBoard**  để có thể tùy chỉnh tại inspector.

  1. **giải thích:** tập hợp các điểm sẽ tính toán vị trí của mỗi điểm để dịch chuyển chúng trong 1 khoảng không gian sao cho không có vị trí nào trong khoảng trống đó bị trống và các điểm không dính vào nhau

## 3 Power
  1. **định nghĩa**: node **Power** trả về giá trị A mũ B trong đó A và B là input nhập vào
  
  1. **đặc điểm, tính chất**: giảm thiểu hoặc khuếch đại giá trị A dựa trên tham số B

  1. **ý nghĩa các cổng**:

| Tên |  Cổng  |      Kiểu      | ràng buộc kiểu   |        ý nghĩa        |
|:---:|:------:|:--------------:|:----------------:|:---------------------:|
|  A  |  Input | Dynamic Vector |       không      |   giá trị đầu vào A   |
|  B  |  Input | Dynamic Vector |       không      |   giá trị đầu vào B   |
| Out | Output | Dynamic Vector |       không      | trả về kết quả A mũ B |

  1. **cách sử dụng:** kéo giá trị output của node trước vào input A, tăng giảm giá trị đầu ra của node bằng cách kéo **power(1)** vào input B

## 4 Sample texture2d 

  1. **định nghĩa**: là node có chức năng trả về giá trị màu Vector 4 RGBA sử dụng trong shader
  
  1. **đặc điểm, tính chất**: sử dụng để lấy giá trị từ **texture2d**  cho về kết cấu màu.

  1. **ý nghĩa các cổng**:

|   Tên   |  Cổng  |      Kiểu     |    ràng buộc kiểu     |              ý nghĩa             |
|:-------:|:------:|:-------------:|:---------------------:|:--------------------------------:|
| Texture |  Input |   Texture 2D  |          None         |      vật liệu 2d để lấy mẫu      |
|    UV   |  Input |    Vector 2   |           UV          |        giá trị uv đầu vào        |
| Sampler |  Input | Sampler State | Default sampler state |          mẫu cho kết cấu         |
|   RGBA  | Output |    Vector 4   |          None         | màu rgba đầu ra có kiểu vector 4 |
|    R    | Output |    Vector 1   |          None         |    nhánh màu red kiểu vector1    |
|    G    | Output |    Vector 1   |          None         |   nhánh màu green kiểu vector1   |
|    B    | Output |    Vector 1   |          None         |    nhánh màu blue kiểu vector1   |
|    A    | Output |    Vector 1   |          None         |   nhánh màu alpha kiểu vector1   |

  1. **cách sử dụng:** Để sử dụng Nút mẫu kết cấu 2D để lấy mẫu **normal map**, hãy đặt tham số thả xuống Loại thành **Normal**

## 5 Multiply

  1. **định nghĩa**: trả về tích của 2 đầu vào là A và B
  
  1. **đặc điểm, tính chất**: nếu đầu vào là vector thì  sẽ trả về tíc vô hướng 2 vector, nếu là ma trận thì trả về 1 ma trận có kích thước tương ứng với đầu vào

  1. **ý nghĩa các cổng**:

| Tên |  Cổng  |   Kiểu  | ràng buộc kiểu   | ý nghĩa |
|:---:|:------:|:-------:|:----------------:|:-------:|
|  A  |  Input | Dynamic |       None       |    A    |
|  B  |  Input | Dynamic |       None       |    B    |
| Out | Output | Dynamic |       None       |   A*B   |

  * **cách sử dụng:** thường dùng để phối giá trị màu, tích vô hướng của vector

## 6 Gradient Noise
  1. **định nghĩa**: tạo ra 1 bề mặt dốc trượt theo 1 phía không đổi
  
  1. **đặc điểm, tính chất**: sử dụng đầu vào Scale(1) để điều khiển mật độ nhiều của Output.

  1. **ý nghĩa các cổng**:

|  Tên  |  Cổng  |   Kiểu   | ràng buộc kiểu   |      ý nghĩa      |
|:-----:|:------:|:--------:|:----------------:|:-----------------:|
| UV    | Input  | Vector 2 | UV               | Vector Uv đầu vào |
| Scale | Input  | Vector 1 | None             | mật độ nhiễu      |
| Out   | Output | Vector 1 | None             | Giá trị UV đầu ra |

  1. **giải thích:** có vai trò  tạo ra 1 map noise để kết hợp với uv đầu vào

## 7 Lerp

  1. **định nghĩa**: trả về giá trị nội suy giữa a và b theo t 
  
  1. **đặc điểm, tính chất**: sử dụng đầu vào  t được giữ trong khoảng từ 0 đến 1, đầu vào A , B có kiểu tùy chỉnh (Vector 1,2,3,4, color,...)

  1. **ý nghĩa các cổng**:

| Tên |  Cổng  |      Kiểu      | ràng buộc kiểu   |                    ý nghĩa                    |
|:---:|:------:|:--------------:|:----------------:|:---------------------------------------------:|
|  A  |  Input | Dynamic Vector |       None       |               giá trị đầu vào A               |
|  B  |  Input | Dynamic Vector |       None       |               giá trị đầu vào B               |
|  T  |  Input | Dynamic Vector |       None       | giá trị điều chỉnh khoảng nội suy giữa A và B |
| Out | Output | Dynamic Vector |       None       |      Giá trị đầu ra tương ứng với A Và B      |

## 8 Tiling and offset

  1. **định nghĩa**: định nghĩa và kết xuất giá trị UV đầu vào  để có thể tùy chỉnh được vị trí gốc và độ co dãn của mật độ UV map.
  
  1. **đặc điểm, tính chất**: sử dụng đầu vào **Offset(2)** để điều khiển việc dịch chuyển của UV map

  1. **ý nghĩa các cổng**:

  |   Tên  |  Cổng  |   Kiểu   | ràng buộc kiểu   |           ý nghĩa           |
|:------:|:------:|:--------:|:----------------:|:---------------------------:|
|   UV   |  Input | Vector 2 |        UV        |      Vector Uv đầu vào      |
| Tiling |  Input | Vector 2 |       None       | độ co dãn theo chiều x và y |
| Offset |  Input | Vector 2 |       None       |          vị trí gốc         |
|   Out  | Output | Vector 2 |       None       |      Giá trị UV đầu ra      |

  1. **cách sử dụng:** trả về map uv, ở đây là map được chạy theo thời gian, và được định  hướng 

  1. **giải thích:** khi truyền UV vào, chúng ta có thể điều chỉnh vị trí bằng ofset và tỉ lệ của uv map bằng tiling

  ## 9 Time

  1. **định nghĩa**: cung cấp quyền truy cập vào các tham số thời gian khac nhau trong shader
  
  1. **đặc điểm, tính chất**: không có input

  1. **ý nghĩa các cổng**:

|      Tên     |  Cổng  |   Kiểu   | ràng buộc kiểu   |                        ý nghĩa                        |
|:------------:|:------:|:--------:|:----------------:|:-----------------------------------------------------:|
|     Time     | Output | Vector 1 |       None       |                      Giá trị time                     |
|   Sine Time  | Output | Vector 1 |       None       |             giá trị time theo biểu đồ sin             |
|  Cosine Time | Output | Vector 1 |       None       |            giá trị time theo biểu đồ cosin            |
|  Delta Time  | Output | Vector 1 |       None       |                quãng thời gian hiện tại               |
| Smooth Delta | Output | Vector 1 |       None       | quãng thời gian quãng thời gian hiên tại được dãn đều |

  1. **cách sử dụng:** lấy output tương ứng với kiểu thời gian cần sử dụng

## 10 UV

  1. **định nghĩa**: cung cáp truyền truy cập vào đỉnh hoặc mảnh
  
  1. **đặc điểm, tính chất**: không có input

  1. **ý nghĩa các cổng**:

  | Tên |  Cổng  |   Kiểu   | ràng buộc kiểu   |       ý nghĩa      |
|:---:|:------:|:--------:|:----------------:|:------------------:|
| Out | Output | Vector 4 |       None       | Tọa độ UV của lưới |

  1. **cách sử dụng:** thêm node và chọn kiểu lấy tọa độ UV 

