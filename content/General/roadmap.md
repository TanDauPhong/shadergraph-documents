+++
title = "Roadmap"
description = ""
weight = 1
+++
{{< lead >}}
This document describes the current status and the upcoming milestones of the KyxBook Webserver project.
{{< /lead >}}

*Updated: Tue, 21 July 2020*

## Current Jobs

| Status | Milestone | Goals | ETA |
| :---: | :--- | :---: | :---: |
| 🚀 | **Quản lý và mua bán license** | 0 / 1 | Sat Aug 8 2020 |
| 🚀 | **Xác thực và phần quyền cho các system** | 0 / 1 | Sat Aug 22 2020 |
| 🚀 | **Viết unit testing và ci/cd]** | 0 / 1 | Tue Sep 12 2020 |

#### Book Service

> Mục tiêu:
* Upload & Manage Book
* Logic & UI User Management
* Logic & UI License Management

| Status | Goal | Labels |
| :---: | :--- | :---: |
| ✔| Upload & Manage Book | `need review` |
| ❌ | Logic License Management | `not start` |
| ❌ | Logic License Management | `not start` |
|  ❌ | UI User Management | `error` |
| ❌ | UI License Management | `not start` |

#### Quản lý và mua bán license

> Mục tiêu:
* Thực hiện được dăng ký và tạo license tự động cho publisher
* Đăng ký tài khoản tự động cho unity account
* Cho phép unity account mua license cho book
* Có UI quản lý cơ bản cho publisher
* Tích hợp mua bán của Zalo

| Status | Goal | Labels |
| :---: | :--- | :---: |
|❌ | Auto configuration account/license cho publisher and unity account | `not start` |
| ❌| Tích hợp Zalo và thực hiện API mua bán | `not start` |
| ❌| API mua bán cơ bản | `not start` |
|❌ | UI quản lý cho publisher | `not start` |

#### Media Service

> Mục tiêu:
* Tích hợp upload to s3 storage
* Api support retrieve cho các internal service

| Status | Goal | Labels |
| :---: | :--- | :---: |
| ✔| Tích hợp upload to s3 storage | `done` |
|❌ | Api support retrieve cho các internal service | `not start` |

#### Notification Service

> Mục tiêu:
* Tích hợp firebase
* In-App-Notification
* UI Manage notification

| Status | Goal | Labels |
| :---: | :--- | :---: |
| ✔| Tích hợp firebase | `need review` |
|  ✔| In-App-Notification | `done` |
|  ✔| UI Manage notification | `done` |



