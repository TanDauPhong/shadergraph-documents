+++
title = "System Sites"
description = ""
weight = 2
+++
{{< lead >}}
List site and account
{{< /lead >}}

## Kyx Identity

> https://identity.kyx.vn

Dùng để đăng nhập, quản lý tài khoản cá nhân

```
    Account: admin
    Password: kyx123JQK#@!
```

## Kyx Identity Admin

> https://identity-admin.kyx.vn

Dùng để đăng nhập, quản lý hệ thống

```
    Account: admin
    Password: kyx123JQK#@!
```

## KyxBook Dashboard

> https://dashboard.book.kyx.vn

Dùng để quản lý profile publisher, upload book

```
    Account: admin
    Password: kyx123JQK#@!
```

## KyxBook Admin Dashboard

> https://dashboard.book.kyx.vn

Dùng để quản lý cấu hình chung như book layout, book grade, send cloud message, approve book

```
    Account: admin
    Password: kyx123JQK#@!
```