+++
title = "Rest Response Format"
description = ""
weight = 3
+++
{{< lead >}}
REST API response format based on some of the best practices
{{< /lead >}}


## Rest API Popular Endpoint Formats

> https://api.kyx.vn/

## Rest API Success Responses

1- GET - Get single item - HTTP Response Code: **200**
> If item is exists
```javascript
    HTTP/1.1 200
    Content-Type: application/json

    {
        "meta":{
            "success": true,
            "code": 200
        },
        "response": {
            "id": 10,
            "name": "shirt",
            "color": "red",
            "price": "$23"
        }
    }
```

> If item isn't exists
```javascript
    HTTP/1.1 200
    Content-Type: application/json

    {
        "meta":{
            "success": true,
            "code": 404
        }
    }
```

2- GET - Get item list - HTTP Response Code: **200**
```javascript
    HTTP/1.1 200
    Content-Type: application/json
    
    {
        "meta": {
            "success": true,
            "code":200,
            "pagination-coun"t: 100,
            "pagination-page": 5,
            "pagination-limit": 20
        },
        "response": [
            {
                "id": 10,
                "name": "shirt",
                "color": "red",
                "price": "$123"
            },
            {
                "id": 11,
                "name": "coat",
                "color": "black",
                "price": "$2300"
            }
        ]
    }
    
```

3- POST - Create a new item - HTTP Response Code: **201**
```javascript
    HTTP/1.1  201
    Content-Type: application/json
    {
        "meta": {
            "success": true
            "code":201
        },
        "response":  {
            "id": 10,
            "name": "coat",
            "color": "black",
            "price": "$2300"
        }, 
    }

```
4- PATCH - Update an item - HTTP Response Code: **200** 

> If updated entity is to be sent after the update

```javascript
    HTTP/1.1  200
    Content-Type: application/json
    
    
    {
        "meta": {
            "success": true,
            "code":200
        },
        "response":  {
            "id": 10,
            "name": "coat",
            "color": "black",
            "price": "$2300"
        }, 
    }
```

> If updated entity is not to be sent after the update

```javascript
    HTTP/1.1  200
    Content-Type: application/json
    {
        "meta": {
            "success": true,
            "code":204
        }
    }
```

5- DELETE - Delete an item - HTTP Response Code: **200**
```javascript
    HTTP/1.1  200

    {
        "meta": {
            "success": true,
            "code":204
        }
    }
```
## Rest API Error Responses

1- GET - HTTP Response Code: **404**

```javascript
    HTTP/1.1  404
    Content-Type: application/json
 
    {
        "meta": {
            "success": false,
            "message": "The item does not exist",
            "code": 404
        }
    }
```
2- DELETE - HTTP Response Code: **404**
```javascript
 
    HTTP/1.1  404
    Content-Type: application/json
 
    {
        "meta": {
            "success": false,
            "message": "The item does not exist",
            "code": 404
        }
    }
```
3- POST -  HTTP Response Code: **400**
```javascript
    HTTP/1.1  400
    Content-Type: application/json
    
    {
        "meta": {
            "success": false,
            "message": "Bad Request",
            "code": 400
        },
        "errors": [
            {
                "message": "Oops! The value is invalid",
                "code": 34,
                "field": "email"
            },
            {
                "message": "Oops! The format is not correct",
                "code": 35,
                "field": "phoneNumber"
            }
        ]
    }
```
4- PATCH -  HTTP Response Code: **400/404**
```javascript
    HTTP/1.1  400
    Content-Type: application/json
    
    {
        "meta": {
            "success": false,
            "message": "Bad Request",
            "code": 400
        },
        "errors": [
            {
                "message": "Oops! The value is invalid",
                "code": 34,
                "field": "email"
            },
            {
                "message": "Oops! The format is not correct",
                "code": 35,
                "field": "phoneNumber"
            }
        ]
    }

    
    
    HTTP/1.1  404
    Content-Type: application/json
 
    {
        "meta": {
            "success": false,
            "message": "The item does not exist",
            "code": 404
        }
    }
```
5- VERB Unauthorized - HTTP Response Code: **401**
```javascript
    HTTP/1.1  401
    Content-Type: application/json
    {
        "meta": {
            "success": false,
            "message": "Authentication credentials were missing or incorrect"
            "code": 401
        }
    }
```
6- VERB Forbidden - HTTP Response Code: **403**
```javascript
    HTTP/1.1  403
    Content-Type: application/json
    
    {
        "meta": {
            "success": false,
            "message": "The request is understood, but it has been refused or access is not allowed"
            "code": 403
        }
    }
```
7- VERB Conflict - HTTP Response Code: **409**
```javascript
    HTTP/1.1  409
    Content-Type: application/json
 
    {
        "meta": {
            "success": false,
            "message": "The request conflict with current state of the server."
            "code": 409
        }
    }
```
8- VERB Too Many Requests - HTTP Response Code: **429**
     
    Luu y: api gateway co the se khong tra ve content, tot nhat la chi check header status

```javascript
    HTTP/1.1  429
    Content-Type: application/json
    
    {
        "meta": {
            "success": false,
            "message": "The request cannot be served due to the rate limit having been exhausted for the resource",
            "code": 429
        }
    }

```
9- VERB Internal Server Error - HTTP Response Code: **500**
```javascript
    HTTP/1.1  500
    Content-Type: application/json
    
    {
        "meta": {
            "success": false,
            "message": "Internal Server Error",
            "code": 500
        }
    }
```
10- VERB Service Unavailable - HTTP Response Code: **503**
```javascript
    HTTP/1.1  503
    Content-Type: application/json
    
    {
        "meta": {
            "success": false,
            "message": "The server is up, but overloaded with requests. Try again later!"
            "code": 503
        }
    }

```
## Validation Error Formats

Validation error formats can be different depending on your requirements. Following are some other popular formats, other than the one used above.

```javascript
    HTTP/1.1  400
    Content-Type: application/json
    
    {
        "meta": {
            "success": false,
            "message": "Bad Request",
            "code": 400
        },
        "errors": [
            {
                "message": "Oops! The value is invalid",
                "code": 34,
                "field": "email"
            },
            {
                "message": "Oops! The format is not correct",
                "code": 35,
                "field": "phoneNumber"
            }
        ]
    }
```


## References
PATCH with partial json can be used for updating the resource: https://tools.ietf.org/html/rfc7396

Avoid using 'X-' in custom headers: https://tools.ietf.org/html/rfc6648