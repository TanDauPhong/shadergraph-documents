+++
title = "KyxBook documentation"
description = ""
+++

{{< lead >}}
KyxBook is solution about ebook for publisher and schools.
{{< /lead >}}


## Features
<div class="row py-3 mb-5">
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-tasks fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Roadmap.
				</h5>
				<p class="card-text text-muted">
					That document describes the current status and the upcoming milestones of the KyxBook Webserver project.
				</p>
                <a href="general/roadmap" class="btn btn-primary">Go</a>
			</div>
		</div>
	</div>
    <div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-sitemap fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Sitemap.
				</h5>
				<p class="card-text text-muted">
					Public front-end service and 
				</p>
                <a href="general/sitemap" class="btn btn-primary">Go</a>
			</div>
		</div>
	</div>
        <div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-dungeon fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Rest Response Format.
				</h5>
				<p class="card-text text-muted">
					REST API response format based on some of the best practices
				</p>
                <a href="general/rest_format"  class="btn btn-primary">Go</a>
			</div>
		</div>
	</div>
</div>

