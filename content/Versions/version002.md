+++
title = "Version 0.0.2"
description = "Version 2 Change Log"
weight = -2
+++

## Unity API

1- Add:

- Api Toggle Notification

2- Update

- Update Route:

	- Change Route: getBooks, profiles, notification...
		+ Create Unity Profile: {{base_book_api_url}}/unity/profile
		+ Get Unity Profile: {{base_book_api_url}}/unity/profile
		+ Get Books: {{base_book_api_url}}/unity/books
		+ Detail Books: {{base_book_api_url}}/unity/books/8
		+ Get Unity Book Favourite: {{base_book_api_url}}/unity/favouritebooks?publisherId=1&paginationPage=0
		+ Get Unity Book Favourite State: {{base_book_api_url}}/unity/favouritebooks/8/state
		+ Toggle Unity Book Favourite State: {{base_book_api_url}}/unity/favouritebooks/8/toggle
		+ Get Unity Cloud Message: {{base_book_api_url}}/unity/notifications?paginationPage=0&clientId={{client_id}}
		+ Mark Unity Cloud Message Read: {{base_notificaiton_api_url}}/19/read (base_notificaiton_api_url = https://api.kyx.vn/notification)
		+ Get Unity Cloud Message State: {{base_notificaiton_api_url}}/19/state

- Update model trả về:
	- In BookModel: 
		+ Change currentVersion -> currentArchiveVersion (version của file download).
		+ Add currentReleaseInfoVersion (version của book info).

## Admin Portal (admin.book.kyx.vn):

- Migrate UI to Ant design
- Add Book Manage

## Editor Portal (dashboard.book.kyx.vn)

- Migrate UI to Ant design
- Thêm tính năng publish sách ở trang detail book



